def measure(num = 1)
    
    final = 0
    time_1 = Time.now
    num.times do 
      yield
    end
    time_2 = Time.now
      final += time_2 - time_1
    final/num
end