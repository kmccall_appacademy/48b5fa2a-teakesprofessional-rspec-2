def reverser
  final_str = []
  yield.split.each do |word|
    final_str << word.reverse
  end
    final_str.join(" ")
end

def adder(num = 1)
  yield + num
end

def repeater(num=1)
  num.times {yield}
end